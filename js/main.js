var maxSlides,
    width = $(window).width();

if (width <  430) {
    maxSlides = 1;
} else  if (width < 767 ) {
    maxSlides = 2;
} else {
    maxSlides = 3;
}

$('.home .bxslider').bxSlider({
    minSlides: 3,
    maxSlides: maxSlides ,
    slideWidth: 285,
    slideMargin: 0,
    auto: true,
});
